using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringFist : MonoBehaviour
{
    public bool can_cut_limb;
    public float temporary_ragdoll_duration;
    public float attack_damage;

    public ParticleSystem[] blood;
    public ParticleSystem[] blood_loop;
    private int current_blood = 0;
    private int current_blood_loop = 0;
    public float can_damage_every_n_seconds;
    public float can_ragdoll_every_n_seconds;
    private bool can_damage = true;
    private bool can_ragdoll = true;

    private GameObject target_object;
    private Rigidbody2D rb;
    private SpringJoint2D joint;

    private Enemy enemy;

    private void Awake()
    {
        enemy = FindObjectOfType<Enemy>();
        joint = GetComponent<SpringJoint2D>();
        //target_object = new GameObject(gameObject.name + "Target");
        //target_object.transform.SetParent(transform.parent);
        //target_object.transform.position = transform.position;
        //target_object.transform.rotation = transform.rotation;

        //transform.SetParent(null);

        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        //transform.position = target.position;
        //transform.rotation = target.rotation;

        //rb.MovePosition(target_object.transform.position);
        //rb.MoveRotation(target_object.transform.rotation);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Weapon"))
        {
            return;
        }

        if (!joint.enabled)
        {
            return;
        }

        if (can_damage)
        {
            can_damage = false;

            Invoke("CanDamageReset", can_damage_every_n_seconds);

            blood[current_blood].transform.position = collision.GetContact(0).point;
            blood[current_blood].transform.parent = collision.transform;
            blood[current_blood].Play();

            //blood[current_blood].transform.LookAt(-collision.GetContact(0).normal);

            current_blood++;

            if (current_blood >= blood.Length)
            {
                current_blood = 0;
            }

            DamageReceiver damage_receiver = collision.gameObject.GetComponent<DamageReceiver>();

            if (damage_receiver != null)
            {
                damage_receiver.ReceiveDamage(attack_damage);
            }

            if (collision.gameObject.CompareTag("LooseLimb") && current_blood_loop < blood_loop.Length && collision.transform.parent != null)
            {
                if (!can_cut_limb)
                {
                    return;
                }
                LooseLimb loose_limb = collision.gameObject.GetComponent<LooseLimb>();

                if (loose_limb != null)
                {
                    loose_limb.LoseTheLimb();
                }
            }
        }

        if (can_ragdoll)
        {
            can_ragdoll = false;

            Invoke("CanRagdollReset", can_ragdoll_every_n_seconds);

            enemy.TemporaryRagdoll(temporary_ragdoll_duration);
        }

    }

    private void CanDamageReset()
    {
        can_damage = true;
    }
    private void CanRagdollReset()
    {
        can_ragdoll = true;
    }
}
