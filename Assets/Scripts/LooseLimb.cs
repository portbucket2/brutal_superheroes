using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LooseLimb : MonoBehaviour
{
    public Avatar parent_avatar;

    [Range(0, 1)]
    public float max_health_before_lose;

    public bool can_lose = false;
    public int can_take_hit = 0;

    public List<HingeJoint2D> disconnect_hinge;
    public List<Rigidbody2D> affected_rb;

    public List<LooseLimb> can_lose_next_limb;

    public PhysicsMaterial2D default_material;

    public UnityEvent OnThisLimbLost;
    private bool limb_lost = false;
    public float bleeding_speed;

    private GameManager game_manager;

    private void Awake()
    {
        game_manager = FindObjectOfType<GameManager>();
    }

    public void CanLose()
    {
        can_lose = true;
    }


    private void Update()
    {
        if (limb_lost && !parent_avatar.is_almost_dead)
        {
            parent_avatar.ReceiveDamage(Time.deltaTime * bleeding_speed);
        }
    }

    public void LoseTheLimb()
    {
        if (parent_avatar.GetHealthPercentage() > max_health_before_lose)
        {
            return;
        }

        if (limb_lost)
        {
            return;
        }

        if (!can_lose)
        {
            return;
        }

        if (can_take_hit > 0)
        {
            can_take_hit--;
            return;
        }

        if (Time.timeScale >= 0.9f)
        {
            Invoke("SlowDownDelayed", 0.5f);
        }

        limb_lost = true;
        GetComponent<DamageReceiver>().enabled = false;

        foreach (HingeJoint2D hinge in disconnect_hinge)
        {
            hinge.connectedBody = null;
            hinge.enabled = false;
        }

        foreach (Rigidbody2D rb in affected_rb)
        {
            rb.sharedMaterial = default_material;
        }

        Invoke("CanLoseNextLimb", 3f);

        OnThisLimbLost?.Invoke();
    }

    private void CanLoseNextLimb()
    {
        foreach (LooseLimb loose_limb in can_lose_next_limb)
        {
            loose_limb.CanLose();
        }
    }


    private void SlowDownDelayed()
    {
        if (game_manager.slow_down_counter > 0)
        {
            game_manager.slow_down_counter--;
            Time.timeScale = 0.3f;
            Invoke("DefaultSpeed", 0.8f);
        }
    }
    
    private void DefaultSpeed()
    {
        Time.timeScale = 1f;
    }

}
