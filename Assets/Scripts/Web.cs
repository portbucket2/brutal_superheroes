using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Web : MonoBehaviour
{
    public bool stick_to_hand;
    public float speed_y;
    public float speed_x;

    private Vector3 target_scale = Vector3.zero;
    private Transform target_transform;
    private Vector3 target_rotation = Vector3.zero;

    private void Awake()
    {
        target_transform = transform;
    }

    private void Update()
    {
        Vector3 scale = transform.localScale;
        scale.x = Mathf.Lerp(scale.x, target_scale.x, Time.deltaTime * speed_x);
        scale.y = Mathf.Lerp(scale.y, target_scale.y, Time.deltaTime * speed_y);

        transform.localScale = scale;
        
        if (stick_to_hand)
        {
            transform.position = target_transform.position;
        }
    }

    public void Shoot(Transform origin, Vector2 direction, bool stick_to_hand = false)
    {
        this.stick_to_hand = stick_to_hand;

        transform.position = origin.position;

        if (stick_to_hand)
        {
            target_transform = origin;
        }

        //Vector2 diff = direction - (Vector2)transform.position;

        target_rotation.z = Mathf.Atan2(-direction.x, direction.y) * Mathf.Rad2Deg;
        transform.eulerAngles = target_rotation;

        target_scale = transform.localScale;
        target_scale.y = (direction - (Vector2)origin.position).magnitude;
        target_scale.x = 0;

        Vector3 scale = transform.localScale;
        
        scale.y = 0;
        scale.x = 1;

        transform.localScale = scale;
    }
}
