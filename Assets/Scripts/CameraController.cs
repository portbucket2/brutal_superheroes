using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject Player;
    public GameObject Enemy;

    public float dist_multiplier;

    public float lower_limit;
    public float upper_limit;

    public float speed;

    private Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        float dist = (Player.transform.position - Enemy.transform.position).magnitude;

        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, Mathf.Clamp(dist * dist_multiplier, lower_limit, upper_limit), Time.deltaTime * speed);
    }
}
